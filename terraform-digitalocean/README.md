# Provisioning VPS dengan Terraform #

1. Generate ssh-key untuk terraform. Jangan pakai password.

    ```
    ssh-keygen -f ~/.ssh/terraform
    ```

    Outputnya seperti ini

    ```
    Generating public/private rsa key pair.
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in .ssh/terraform.
    Your public key has been saved in .ssh/terraform.pub.
    The key fingerprint is:
    SHA256:OVrUlytWTNb6HJt3JMwfayRiFvkkPjGyFZSufXZvu28 endymuhardin@Endys-MacBook-Pro.local
    The key's randomart image is:
    +---[RSA 3072]----+
    |          .o*.   |
    |         o % o.  |
    |        . B %+   |
    |       . o X.+=o.|
    |        S B +ooBo|
    |       o + o o=++|
    |      .     o o.o|
    |                E|
    |               ==|
    +----[SHA256]-----+

    ```

2. Daftarkan public key ssh di DO

    Tampilkan dulu public keynya dan copy ke clipboard

    ```
    cat ~/.ssh/terraform.pub 
    ```

    Daftarkan di website DO, bagian Settings > Security

    [![Daftarkan SSH Key](./img/ssh-key-do.png)](./img/ssh-key-do.png)

3. Ambil nama ssh-key laptop kita yang sudah didaftarkan di DO

    ```
    doctl compute ssh-key list
    ```

4. Generate API Token di website Digital Ocean

    [![Generate API Token](./img/generate-do-token.png)](./img/generate-do-token.png)

    Misalnya outputnya adalah sebagai berikut

    ```
    d612d83dd9173e2e0342f1b609947522f6f0fd7743f987a540a3ac79eba86e28
    ```

5. Pasang nama ssh-key di file `provider.tf`. Sisanya biarkan saja default

    ```
    data "digitalocean_ssh_key" "terraform" {
       name = "terraform"
    }
    ```

6. Jalankan `terraform init`. Outputnya seperti ini

    ```
    Initializing the backend...

    Initializing provider plugins...
    - Finding digitalocean/digitalocean versions matching "1.22.2"...
    - Installing digitalocean/digitalocean v1.22.2...
    - Installed digitalocean/digitalocean v1.22.2 (signed by a HashiCorp partner, key ID F82037E524B9C0E8)

    Partner and community providers are signed by their developers.
    If you'd like to know more about provider signing, you can read about it here:
    https://www.terraform.io/docs/cli/plugins/signing.html

    Terraform has created a lock file .terraform.lock.hcl to record the provider
    selections it made above. Include this file in your version control repository
    so that Terraform can guarantee to make the same selections by default when
    you run "terraform init" in the future.
    ```

7. Buat file konfigurasi VPS yang akan dibuat, misalnya `www-1.tf`

8. Generate deployment plan

    ```
    terraform plan -out=infra.out
    ```

    Outputnya seperti ini

    ```
    var.do_token
    Enter a value: d612d83dd9173e2e0342f1b609947522f6f0fd7743f987a540a3ac79eba86e28

    var.pvt_key
    Enter a value: ~/.ssh/terraform


    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
    + create

    Terraform will perform the following actions:

    # digitalocean_droplet.www-1 will be created
    + resource "digitalocean_droplet" "www-1" {
        + backups              = false
        + created_at           = (known after apply)
        + disk                 = (known after apply)
        + id                   = (known after apply)
        + image                = "ubuntu-20-10-x64"
        + ipv4_address         = (known after apply)
        + ipv4_address_private = (known after apply)
        + ipv6                 = false
        + ipv6_address         = (known after apply)
        + ipv6_address_private = (known after apply)
        + locked               = (known after apply)
        + memory               = (known after apply)
        + monitoring           = false
        + name                 = "www-1"
        + price_hourly         = (known after apply)
        + price_monthly        = (known after apply)
        + private_networking   = true
        + region               = "sgp1"
        + resize_disk          = true
        + size                 = "s-1vcpu-1gb"
        + ssh_keys             = [
            + "714736",
            ]
        + status               = (known after apply)
        + urn                  = (known after apply)
        + vcpus                = (known after apply)
        + volume_ids           = (known after apply)
        + vpc_uuid             = (known after apply)
        }

    Plan: 1 to add, 0 to change, 0 to destroy.

    ------------------------------------------------------------------------

    This plan was saved to: infra.out

    To perform exactly these actions, run the following command to apply:
        terraform apply "infra.out"
    ```

9. Jalankan plan

    ```
    terraform apply "infra.out"
    ```

    Outputnya seperti ini 

    ```
    digitalocean_droplet.www-1: Creating...
    digitalocean_droplet.www-1: Still creating... [10s elapsed]
    digitalocean_droplet.www-1: Still creating... [20s elapsed]
    digitalocean_droplet.www-1: Still creating... [30s elapsed]
    digitalocean_droplet.www-1: Still creating... [40s elapsed]
    digitalocean_droplet.www-1: Provisioning with 'remote-exec'...
    digitalocean_droplet.www-1 (remote-exec): Connecting to remote host via SSH...
    digitalocean_droplet.www-1 (remote-exec):   Host: 206.189.85.129
    digitalocean_droplet.www-1 (remote-exec):   User: root
    digitalocean_droplet.www-1 (remote-exec):   Password: false
    digitalocean_droplet.www-1 (remote-exec):   Private key: true
    digitalocean_droplet.www-1 (remote-exec):   Certificate: false
    digitalocean_droplet.www-1 (remote-exec):   SSH Agent: true
    digitalocean_droplet.www-1 (remote-exec):   Checking Host Key: false
    digitalocean_droplet.www-1 (remote-exec):   Target Platform: unix
    .....
    .....
    digitalocean_droplet.www-1: Creation complete after 2m33s [id=237990892]

    Apply complete! Resources: 1 added, 0 changed, 1 destroyed.

    The state of your infrastructure has been saved to the path
    below. This state is required to modify and destroy your
    infrastructure, so keep it safe. To inspect the complete state
    use the `terraform show` command.

    State path: terraform.tfstate
    ```

10. Melihat hasilnya

    ```
    terraform show
    ```

    Outputnya seperti ini

    ```
    # data.digitalocean_ssh_key.terraform:
    data "digitalocean_ssh_key" "terraform" {
        fingerprint = "46:eb:ea:27:5b:b9:c5:16:17:6f:57:b1:d7:a8:b8:76"
        id          = "29857468"
        name        = "terraform"
        public_key  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCjM0YzO9QYGoCuz010LAx/m+QO35IG/m0B0qsuL+gFSp4It0bX5uyWPMDTZs6tni/AgVdQNsHFo1NjhYlb1ShN4QwgRcEdiVQYoj2H4oEGvrGUmWjWV9zDVb87AHnZmiDbOV+kTkQ9+nmlosvLhvWw404wr2zyYtaql3PzuUpF2NORohyLE5vemgFptC4w3FLy7Bc0aykFntDF/EKCl/9/mC5VqkB8DCbPGVa9AEpBEuDDCIvcOeOYZwsvftdcMLIkVYRiQmrQSyY7Ney9Jex1S115xtcSZ3RFgT7i/xJWZ3/egJMR+FxvtOlC1i0ZLgFsVEqH5i3xHXjzZzazG80SG7QwtKYngbllukLitiFvM8ZqJ2/wwL00rAD5xDSxf7szv7C49II09fg8LrSvFnfCy9Ap6OQ5/eNs3lYWmLKv4NwSOPDb9cq4c1LzXK7H+/rZxgOHr/PR59/fjNdxsObGRS8rVRpseDe7hcCLg2m2s9JW8fTwwVQFtgLOSccWjms= endymuhardin@Endys-MacBook-Pro.local"
    }

    # digitalocean_droplet.www-1:
    resource "digitalocean_droplet" "www-1" {
        backups              = false
        created_at           = "2021-03-22T04:08:16Z"
        disk                 = 25
        id                   = "237990892"
        image                = "ubuntu-20-10-x64"
        ipv4_address         = "206.189.85.129"
        ipv4_address_private = "10.130.0.3"
        ipv6                 = false
        locked               = false
        memory               = 1024
        monitoring           = false
        name                 = "www-1"
        price_hourly         = 0.00744
        price_monthly        = 5
        private_networking   = true
        region               = "sgp1"
        resize_disk          = true
        size                 = "s-1vcpu-1gb"
        ssh_keys             = [
            "29857468",
        ]
        status               = "active"
        urn                  = "do:droplet:237990892"
        vcpus                = 1
        volume_ids           = []
        vpc_uuid             = "27f2bec3-dc83-11e8-beb4-3cfdfea9fb81"
    }
    ```