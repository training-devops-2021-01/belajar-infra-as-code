node 'puppetagent-1' {
  class { 'apache': }             # use apache module
  apache::vhost { 'puppet.localdomain':  # define vhost resource
    port    => '80',
    docroot => '/var/www/html'
  }
}