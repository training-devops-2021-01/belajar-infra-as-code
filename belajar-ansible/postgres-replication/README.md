# Setup Replikasi PostgreSQL dengan Ansible #

1. Buat droplet digital ocean untuk menjadi primary

    ```
    doctl compute droplet \
          create pg-primary \
          --size s-1vcpu-2gb \
          --image centos-8-x64 \
          --region sgp1 \
          --tag-name ansible-pgrepl \
          --ssh-keys 714736
    ```

2. Buat droplet digital ocean untuk menjadi replica

    ```
    doctl compute droplet \
          create pg-replica \
          --size s-1vcpu-2gb \
          --image centos-8-x64 \
          --region sgp1 \
          --tag-name ansible-pgrepl \
          --ssh-keys 714736
    ```

3. Cek alamat IP primary dan replica

    ```
    doctl compute droplet list --format "ID,Name,PublicIPv4,Tags"
    ```

4. Edit file `hosts.yml`, sesuaikan alamat IP dengan yang ada di droplet

5. Set environment variabel `PG_REPL_PASSWD` untuk menjadi password replikasi

    ```
    export PG_REPL_PASSWD='replikasi4321'
    ```

5. Jalankan ansible playbook untuk setup primary dan replica

    ```
    ansible-playbook -i hosts.yml pg_setup.yml
    ```


6. Hapus droplet setelah tidak dipakai

    ```
    doctl compute droplet rm --tag-name ansible-pgrepl
    ```

## Referensi ##

* https://www.epilis.gr/en/blog/2019/10/08/postgresql-ha-setup-using-ansible/