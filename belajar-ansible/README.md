# Intro Ansible #

1. Instalasi MacOS

    ```
    pip3 install --user ansible
    ```

    Verifikasi hasilnya

    ```
    ansible --version
    ```

    Outputnya:

    ```
    ansible 2.10.7
    config file = None
    configured module search path = ['/Users/endymuhardin/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
    ansible python module location = /Users/endymuhardin/Library/Python/3.9/lib/python/site-packages/ansible
    executable location = /Users/endymuhardin/Library/Python/3.9/bin/ansible
    python version = 3.9.1 (default, Feb  6 2021, 10:23:48) [Clang 12.0.0 (clang-1200.0.32.29)]
    ```

2. Buat 2 VPS untuk menjadi `puppetserver` dan `puppetagent-1`

    * `puppetserver`

    ```
    doctl compute droplet \
          create puppetserver \
          --size s-1vcpu-2gb \
          --image ubuntu-20-04-x64 \
          --region sgp1 \
          --tag-name ansible-puppet \
          --ssh-keys 714736
    ```

    * `puppetagent-1`
    
    ```
    doctl compute droplet \
          create puppetagent-1 \
          --size s-1vcpu-2gb \
          --image ubuntu-20-04-x64 \
          --region sgp1 \
          --tag-name ansible-puppet \
          --ssh-keys 714736
    ```

    * SSH Key ID bisa dilihat dengan perintah berikut

    ```
    doctl compute ssh-key list
    ```

2. Buat inventory file `hosts.yml`

3. Test login dan menampilkan data di host yang telah didaftarkan di inventory

    ```
    ansible -i hosts.yml <hostname> -m ansible.builtin.setup
    ```

4. Menginstal puppetserver dengan ansible

    ```
    ansible-playbook -i hosts.yml puppetserver.yml
    ```

5. Menginstal puppetagent dengan ansible

    ```
    ansible-playbook -i hosts.yml puppetagents.yml
    ```

6. Lihat request sertifikat dari agent

    ```
    /opt/puppetlabs/bin/puppetserver ca list
    ```

7. Sign request dari agent

    ```
    /opt/puppetlabs/bin/puppetserver ca sign --all
    ```

8. Deploy konfigurasi ke puppet server dengan cara mengedit file `/etc/puppetlabs/code/environments/production/manifests/site.pp`. Buat konfigurasi deployment untuk masing-masing node seperti ini

   ```
   node 'puppetagent-1' {
        class { 'apache': }             # use apache module
        apache::vhost { 'puppet.localdomain':  # define vhost resource
            port    => '80',
            docroot => '/var/www/html'
        }
    }
   ```